package teamdech.growableresources.client.sounds;


import net.minecraft.client.Minecraft;
import net.minecraft.world.World;

public enum GRSound {

    NULL("", ""); // TODO

    String soundName, soundExtension;

    GRSound(String soundName, String soundExtension) {
        this.soundName = soundName;
        this.soundExtension = soundExtension;
    }

    public void playClientSide(double x, double y, double z, float volume, float pitch) {
        Minecraft.getMinecraft().sndManager.playSound(String.format("growableresources:%s", this.soundName), (float) x, (float) y, (float) z, volume, pitch);
    }

    public void playServerSide(World world, double x, double y, double z, float volume, float pitch) {
        world.playSoundEffect(x, y, z, String.format("growableresources:%s", this.soundName), volume, pitch);
    }
}
