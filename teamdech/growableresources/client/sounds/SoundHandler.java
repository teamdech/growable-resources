package teamdech.growableresources.client.sounds;

import net.minecraftforge.client.event.sound.SoundLoadEvent;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.ForgeSubscribe;

public class SoundHandler {

    public SoundHandler() {
        MinecraftForge.EVENT_BUS.register(this);
    }

    @ForgeSubscribe
    public void onSoundLoad(SoundLoadEvent event) {
        for (GRSound sound : GRSound.values()) {
            addSound(event, sound.soundName, sound.soundExtension);
        }
    }

    private void addSound(SoundLoadEvent event, String soundName, String soundExtension) {
        event.manager.soundPoolSounds.addSound(String.format("growableresources:%s.%s", soundName, soundExtension));
    }
}
