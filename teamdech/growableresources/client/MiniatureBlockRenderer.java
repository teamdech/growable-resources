package teamdech.growableresources.client;

import net.minecraft.block.Block;
import net.minecraft.client.renderer.RenderBlocks;
import net.minecraft.client.renderer.Tessellator;
import net.minecraft.util.Icon;
import net.minecraft.world.IBlockAccess;

import org.lwjgl.opengl.GL11;

import teamdech.growableresources.blocks.Blocks;
import teamdech.growableresources.blocks.TileEntityMiniatureBlock;
import teamdech.growableresources.proxies.ClientProxy;

import cpw.mods.fml.client.registry.ISimpleBlockRenderingHandler;

public class MiniatureBlockRenderer implements ISimpleBlockRenderingHandler {
    
    float minU, maxU, minV, maxV;

    @Override
    public void renderInventoryBlock(Block block, int metadata, int modelID, RenderBlocks renderer) {
    }

    @Override
    public boolean renderWorldBlock(IBlockAccess world, int x, int y, int z, Block block, int modelId, RenderBlocks renderer) {
	Tessellator t = Tessellator.instance;
		
	TileEntityMiniatureBlock tile = (TileEntityMiniatureBlock) world.getBlockTileEntity(x, y, z);
	if (tile == null) return true;
	if (tile.blockID == 0 || Block.blocksList[tile.blockID] == null) tile.blockID = 1;

	t.setBrightness(Blocks.blockMiniBlock.getMixedBrightnessForBlock(world, x, y, z));
	t.setColorOpaque_F(1F, 1F, 1F);

	bindTexture(Block.blocksList[tile.blockID].getIcon((Block.blocksList[tile.blockID].getRenderType() == 16 ? 1 : 0), tile.damage), t);
	
	t.addVertexWithUV(x + 0.5F - 0.2F, y, z + 0.5F - 0.2F, minU, minV);
	t.addVertexWithUV(x + 0.5F + 0.2F, y, z + 0.5F - 0.2F, maxU, minV);
	t.addVertexWithUV(x + 0.5F + 0.2F, y, z + 0.5F + 0.2F, maxU, maxV);
	t.addVertexWithUV(x + 0.5F - 0.2F, y, z + 0.5F + 0.2F, minU, maxV);
	
	bindTexture(Block.blocksList[tile.blockID].getIcon((Block.blocksList[tile.blockID].getRenderType() == 16 ? 0 : 1), tile.damage), t);

	t.addVertexWithUV(x + 0.5F - 0.2F, y + 0.4F, z + 0.5F - 0.2F, minU, minV);
	t.addVertexWithUV(x + 0.5F - 0.2F, y + 0.4F, z + 0.5F + 0.2F, minU, maxV);
	t.addVertexWithUV(x + 0.5F + 0.2F, y + 0.4F, z + 0.5F + 0.2F, maxU, maxV);
	t.addVertexWithUV(x + 0.5F + 0.2F, y + 0.4F, z + 0.5F - 0.2F, maxU, minV);

	bindTexture(Block.blocksList[tile.blockID].getIcon(2, tile.damage), t);

	t.addVertexWithUV(x + 0.5F + 0.2F, y + 0.4F, z + 0.5F - 0.2F, maxU, minV);
	t.addVertexWithUV(x + 0.5F + 0.2F, y + 0.4F, z + 0.5F + 0.2F, minU, minV);
	t.addVertexWithUV(x + 0.5F + 0.2F, y, z + 0.5F + 0.2F, minU, maxV);
	t.addVertexWithUV(x + 0.5F + 0.2F, y, z + 0.5F - 0.2F, maxU, maxV);

	bindTexture(Block.blocksList[tile.blockID].getIcon(3, tile.damage), t);
	
	t.addVertexWithUV(x + 0.5F + 0.2F, y + 0.4F, z + 0.5F + 0.2F, maxU, minV);
	t.addVertexWithUV(x + 0.5F - 0.2F, y + 0.4F, z + 0.5F + 0.2F, minU, minV);
	t.addVertexWithUV(x + 0.5F - 0.2F, y, z + 0.5F + 0.2F, minU, maxV);
	t.addVertexWithUV(x + 0.5F + 0.2F, y, z + 0.5F + 0.2F, maxU, maxV);

	bindTexture(Block.blocksList[tile.blockID].getIcon(4, tile.damage), t);
	
	t.addVertexWithUV(x + 0.5F - 0.2F, y + 0.4F, z + 0.5F + 0.2F, maxU, minV);
	t.addVertexWithUV(x + 0.5F - 0.2F, y + 0.4F, z + 0.5F - 0.2F, minU, minV);
	t.addVertexWithUV(x + 0.5F - 0.2F, y, z + 0.5F - 0.2F, minU, maxV);
	t.addVertexWithUV(x + 0.5F - 0.2F, y, z + 0.5F + 0.2F, maxU, maxV);

	bindTexture(Block.blocksList[tile.blockID].getIcon(5, tile.damage), t);
	
	t.addVertexWithUV(x + 0.5F - 0.2F, y + 0.4F, z + 0.5F - 0.2F, maxU, minV);
	t.addVertexWithUV(x + 0.5F + 0.2F, y + 0.4F, z + 0.5F - 0.2F, minU, minV);
	t.addVertexWithUV(x + 0.5F + 0.2F, y, z + 0.5F - 0.2F, minU, maxV);
	t.addVertexWithUV(x + 0.5F - 0.2F, y, z + 0.5F - 0.2F, maxU, maxV);
		
	return true;
    }

    public void bindTexture(Icon icon, Tessellator t) {
	t.setColorOpaque_F(1F, 1F, 1F);
	minU = icon.getMinU();
	maxU = icon.getMaxU();
	minV = icon.getMinV();
	maxV = icon.getMaxV();
	if (icon.getIconName().equals("leaves_oak")) t.setColorOpaque_I(Block.leaves.getRenderColor(0));
	if (icon.getIconName().equals("leaves_spruce")) t.setColorOpaque_I(Block.leaves.getRenderColor(1));
	if (icon.getIconName().equals("leaves_birch")) t.setColorOpaque_I(Block.leaves.getRenderColor(2));
	if (icon.getIconName().equals("grass_top")) t.setColorOpaque_I(Block.grass.getRenderColor(0));
	
    }

    @Override
    public boolean shouldRender3DInInventory() {
	return false;
    }

    @Override
    public int getRenderId() {
	return ClientProxy.miniBlockRendererID;
    }
}
