package teamdech.growableresources.recipe;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.CraftingManager;
import teamdech.growableresources.blocks.Blocks;
import teamdech.growableresources.items.Items;

public class Recipes {
    public static InfusionRecipeHandler infusionCrafting;

    public static void addRecipes() {
        CraftingManager.getInstance().addRecipe(new ItemStack(Blocks.blockFertilizedSoil, 2, 0), "BDB", "DWD", "BDB", 'B', new ItemStack(Item.dyePowder, 1, 15), 'D', Block.dirt, 'W', Item.bucketWater);
        CraftingManager.getInstance().addRecipe(new ItemStack(Blocks.blockShredder, 1, 0), "SRS", "PIP", "SRS", 'S', Block.stone, 'P', Item.pickaxeDiamond, 'I', Block.blockIron, 'R', Item.redstone);
        CraftingManager.getInstance().addRecipe(new ItemStack(Blocks.blockInfuser, 1, 0), "BSB", "SIS", "BSB", 'B', Item.blazeRod, 'S', Block.stone, 'I', Items.itemInfusionCore);
        CraftingManager.getInstance().addRecipe(new ItemStack(Blocks.blockMiniBlock, 4, 0), "S", 'S', Block.stoneSingleSlab);
        
        CraftingManager.getInstance().addRecipe(new ItemStack(Items.itemInfusionCore, 1, 0), "GDG", "DRD", "GDG", 'G', Item.ingotGold, 'D', Item.diamond, 'R', Items.itemReedRoots);
        CraftingManager.getInstance().addShapelessRecipe(new ItemStack(Items.itemInfusionFuel, 2, 0), Item.blazeRod, new ItemStack(Item.coal, 1, Short.MAX_VALUE), Item.gunpowder, Item.blazePowder);
        CraftingManager.getInstance().addShapelessRecipe(new ItemStack(Items.itemGuide), Item.book, Item.reed);

        CraftingManager.getInstance().addRecipe(new ItemStack(Items.itemInfoTool), "GGG", "GQG", "SRS", 'G', Block.thinGlass, 'Q', Item.netherQuartz, 'S', Block.stone, 'R', Item.redstone);

        infusionCrafting = new InfusionRecipeHandler();
        new InfusionTooltipHandler();
        GameRegistry.registerFuelHandler(new GRFuelHandler());
    }
}