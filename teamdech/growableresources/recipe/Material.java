package teamdech.growableresources.recipe;

import net.minecraft.item.ItemStack;

public class Material {
    public int product, possibility, id, meta;

    public Material(int product, int possibility, ItemStack stack) {
        this.product = product;
        this.possibility = possibility;
        id = stack.itemID;
        meta = stack.getItemDamage();
    }
}
