package teamdech.growableresources.recipe;

import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import teamdech.growableresources.items.Items;
import teamdech.growableresources.resources.Resource;
import teamdech.growableresources.resources.Resources;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

public class InfusionRecipeHandler {
    public int[] infusionBaseValues;
    public ArrayList<Material> materials;

    public InfusionRecipeHandler() {
        infusionBaseValues = new int[Resources.getNextAvailableID()];

        for (int i = 0; i < Resources.getNextAvailableID(); i++) {
            infusionBaseValues[i] = Resources.getResourceByID(i).infusionBaseValue;
        }

        materials = new ArrayList<Material>();

        add(1, 5, new ItemStack(Item.ingotIron));
        add(1, 50, new ItemStack(Block.blockIron));
        add(2, 5, new ItemStack(Item.ingotGold));
        add(2, 50, new ItemStack(Block.blockGold));
        add(3, 4, new ItemStack(Item.diamond));
        add(3, 42, new ItemStack(Block.blockDiamond));
        add(4, 4, new ItemStack(Item.emerald));
        add(4, 40, new ItemStack(Block.blockEmerald));
        add(5, 8, new ItemStack(Item.dyePowder, 1, 4));
        add(5, 75, new ItemStack(Block.blockLapis));
        add(6, 10, new ItemStack(Item.glowstone));
        add(6, 45, new ItemStack(Block.glowStone));
        add(6, 3, new ItemStack(Block.netherrack));
        add(7, 15, new ItemStack(Item.clay));
        add(7, 60, new ItemStack(Block.blockClay));
        add(7, 40, new ItemStack(Block.hardenedClay));
        add(7, 5, new ItemStack(Block.sand));
        add(8, 10, new ItemStack(Item.netherQuartz));
        add(8, 45, new ItemStack(Block.blockNetherQuartz));
        add(8, 3, new ItemStack(Block.netherrack));
        add(9, 10, new ItemStack(Block.mushroomCapBrown));
        add(9, 50, new ItemStack(Block.mycelium));
        add(10, 10, new ItemStack(Block.mushroomCapRed));
        add(10, 50, new ItemStack(Block.mycelium));
        add(11, 5, new ItemStack(Item.enderPearl));
        add(11, 2, new ItemStack(Block.obsidian));
        add(12, 8, new ItemStack(Item.blazeRod));
        add(12, 4, new ItemStack(Item.blazePowder));
        add(13, 5, new ItemStack(Item.skull, 1, 1));
        add(13, 2, new ItemStack(Item.coal));
        add(13, 2, new ItemStack(Item.bone));
        add(14, 10, new ItemStack(Item.cookie));
        add(15, 7, new ItemStack(Item.slimeBall));
        add(16, 10, new ItemStack(Item.potato));
        add(16, 13, new ItemStack(Item.bakedPotato));
        add(17, 10, new ItemStack(Item.carrot));
        add(18, 12, new ItemStack(Item.wheat));
        add(18, 80, new ItemStack(Block.hay));
        add(19, 8, new ItemStack(Item.ghastTear));
        add(19, 1, new ItemStack(Block.netherrack));
        add(20, 10, new ItemStack(Item.leather));
        add(20, 7, new ItemStack(Item.beefRaw));
        add(21, 6, new ItemStack(Item.expBottle));
        add(21, 1, new ItemStack(Block.bookShelf));
        add(22, 10, new ItemStack(Item.egg));
        add(23, 12, new ItemStack(Item.appleRed));
        add(23, 80, new ItemStack(Item.appleGold));
        add(24, 6, new ItemStack(Block.obsidian));
        add(24, 16, new ItemStack(Item.enderPearl));
        add(25, 15, new ItemStack(Block.slowSand));
        add(25, 4, new ItemStack(Block.netherrack));
        add(26, 8, new ItemStack(Block.cloth));
        add(27, 8, new ItemStack(Item.dyePowder));
        add(27, 16, new ItemStack(Block.obsidian));
        add(28, 10, new ItemStack(Item.silk));
        add(28, 45, new ItemStack(Block.cloth));
        add(29, 12, new ItemStack(Item.redstone));
        add(29, 120, new ItemStack(Block.blockRedstone));
        add(29, 50, new ItemStack(Item.redstoneRepeater));
        add(30, 10, new ItemStack(Item.porkRaw));
        add(30, 12, new ItemStack(Item.porkCooked));
        add(31, 10, new ItemStack(Item.beefRaw));
        add(31, 12, new ItemStack(Item.beefCooked));
        add(31, 15, new ItemStack(Item.leather));
        add(32, 10, new ItemStack(Item.chickenRaw));
        add(32, 12, new ItemStack(Item.chickenCooked));
        add(32, 15, new ItemStack(Item.egg));
        add(33, 6, new ItemStack(Item.gunpowder));
        add(33, 4, new ItemStack(Block.obsidian));
        add(34, 4, new ItemStack(Item.coal));
        add(34, 45, new ItemStack(Block.coalBlock));
        add(34, 3, new ItemStack(Item.coal, 1, 1));
    }

    public ItemStack getOutput(ItemStack input, ItemStack[] materials) {
        ArrayList<Integer> results = new ArrayList<Integer>();

        if (input == null) return null;

        for (int i = 0; i < Resources.getResourceByID(input.getItemDamage()).infusionBaseValue; i++) {
            results.add(input.getItemDamage());
        }

        Iterator<Material> i = Recipes.infusionCrafting.materials.iterator();
        while (i.hasNext()) {
            Material temp = i.next();

            for (int j = 0; j < 8; j++) {
                if (materials[j] != null) {
                    if (temp.id == materials[j].itemID && temp.meta == materials[j].getItemDamage() && validMaterial(temp, input.getItemDamage())) {
                        for (int k = 0; k < temp.possibility; k++) {
                            results.add(temp.product);
                        }
                    }
                }
            }
        }

        int len = results.toArray().length;
        Integer[] resourceIDs = new Integer[len];
        resourceIDs = results.toArray(resourceIDs);
        int resourceID = resourceIDs[new Random().nextInt(len)];
        return new ItemStack(Items.itemReedRoots, 1, resourceID);
    }

    public void add(int product, int probability, ItemStack stack) {
        materials.add(new Material(product, probability, stack));
    }

    public boolean validMaterial(Material material, int baseResource) {
        Iterator<Resource> i = Resources.resourceList.iterator();

        while (i.hasNext()) {
            Resource temp = i.next();
            if (material.product == temp.id && temp.origin == baseResource) {
                return true;
            }
        }
        return false;
    }
}
