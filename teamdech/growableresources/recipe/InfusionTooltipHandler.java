package teamdech.growableresources.recipe;

import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.ForgeSubscribe;
import net.minecraftforge.event.entity.player.ItemTooltipEvent;
import teamdech.growableresources.gui.ContainerInfuser;
import teamdech.growableresources.items.Items;
import teamdech.growableresources.resources.Resource;
import teamdech.growableresources.resources.Resources;

import java.util.Iterator;

public class InfusionTooltipHandler {
    public InfusionTooltipHandler() {
	MinecraftForge.EVENT_BUS.register(this);
    }

    @ForgeSubscribe
    public void getItemTooltip(ItemTooltipEvent event) {
	if (event.entityPlayer != null && event.itemStack != null) {
	    if (event.entityPlayer.openContainer instanceof ContainerInfuser) {
		if (event.itemStack.itemID == Items.itemReedRoots.itemID) {
		    event.toolTip.add("Infusion Resistance: " + Recipes.infusionCrafting.infusionBaseValues[event.itemStack.getItemDamage()]);

		    boolean first = true;

		    Iterator<Resource> i = Resources.resourceList.iterator();

		    while (i.hasNext()) {
			Resource temp = i.next();
			if (temp.active && temp.origin == event.itemStack.getItemDamage()) {
			    if (first) {
				event.toolTip.add("Can be infused to:");
				first = false;
			    }
			    event.toolTip.add("- " + temp.name + " Reeds");
			}
		    }
		} else {
		    boolean first = true;

		    Iterator<Material> i = Recipes.infusionCrafting.materials.iterator();

		    while (i.hasNext()) {
			Material temp = i.next();
			if (temp.id == event.itemStack.itemID && temp.meta == event.itemStack.getItemDamage()) {
			    if (first) {
				event.toolTip.add("Possible infusion results:");
				first = false;
			    }
			    event.toolTip.add("- " + Resources.getResourceByID(temp.product).name + " Reeds (" + temp.possibility + ")");
			}
		    }
		}
	    }
	}
    }
}
