package teamdech.growableresources.util;

import net.minecraftforge.common.Configuration;

import java.io.File;

public class GRConfig {
    public static int blockReedsID = 1200, blockFertilizedSoilID = 1201, blockShredderID = 1202, blockInfuserID = 1203, blockMiniBlockID = 1211;
    public static int itemReedRootsID = 1204, itemReedsID = 1205, itemInfusionFuelID = 1206, itemInfusionCoreID = 1207, itemTickerID = 1208, itemGuideID = 1209, itemInfoToolID = 1210;
    public static int tickDeviceTicksPerUse = 1;
    public static boolean enableParticleFX = true, enableColoredNames = true, enableOverpoweredFeatures = false;

    private Configuration configuration;

    public GRConfig loadConfig(File file) {
        configuration = new Configuration(file);

        blockReedsID = configuration.get("IDs", "BlockReedsID", 1200).getInt();
        blockFertilizedSoilID = configuration.get("IDs", "BlockFertilizedSoilID", 1201).getInt();
        blockShredderID = configuration.get("IDs", "BlockShredderID", 1202).getInt();
        blockInfuserID = configuration.get("IDs", "BlockInfuserID", 1203).getInt();
        blockMiniBlockID = configuration.get("IDs", "BlockMiniBlockID", 1211).getInt();

        itemReedRootsID = configuration.get("IDs", "ItemReedRootsID", 1204).getInt();
        itemReedsID = configuration.get("IDs", "ItemReedsID", 1205).getInt();
        itemInfusionFuelID = configuration.get("IDs", "ItemInfusionFuelID", 1206).getInt();
        itemInfusionCoreID = configuration.get("IDs", "ItemInfusionCoreID", 1207).getInt();
        itemTickerID = configuration.get("IDs", "ItemTickerID", 1208).getInt();
        itemGuideID = configuration.get("IDs", "ItemGuideID", 1209).getInt();
        itemInfoToolID = configuration.get("IDs", "ItemInfoToolID", 1210).getInt();

        enableParticleFX = configuration.get("Visual", "EnableParticleFX", true).getBoolean(true);
        enableColoredNames = configuration.get("Visual", "EnableColoredNames", true).getBoolean(true);

        tickDeviceTicksPerUse = configuration.get("OP", "tickingDeviceTicksPerUse", 1).getInt(1);
        enableOverpoweredFeatures = configuration.get("OP", "enableOverpoweredFeatures", false).getBoolean(false);

        return this;
    }

    public void saveConfig() {
        configuration.save();
    }
}
