package teamdech.growableresources.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.StepSound;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import teamdech.growableresources.GrowableResources;

public class BlockFertilizedSoil extends Block {
    public BlockFertilizedSoil(int id, Material material) {
        super(id, material);
        setUnlocalizedName("fertilizedSoil");
        setCreativeTab(GrowableResources.tabGrowableResources);
        setStepSound(new StepSound("gravel", 1.0F, 0.75F));
    }

    @Override
    public void registerIcons(IconRegister iconRegister) {
        blockIcon = iconRegister.registerIcon("growableresources:fertilizedSoil");
    }
}