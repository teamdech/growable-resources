package teamdech.growableresources.blocks;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.tileentity.TileEntity;
import net.minecraftforge.common.MinecraftForge;
import teamdech.growableresources.GrowableResources;
import teamdech.growableresources.util.GRConfig;

public class Blocks {
    public static Block blockReeds, blockFertilizedSoil, blockShredder, blockInfuser, blockMiniBlock;

    public static void registerBlocks() {
        blockReeds = new BlockResourceReeds(GRConfig.blockReedsID).setUnlocalizedName("blockReeds");
        GameRegistry.registerBlock(blockReeds, "blockReeds");
        LanguageRegistry.addName(blockReeds, "Resource Reeds");

        blockFertilizedSoil = new BlockFertilizedSoil(GRConfig.blockFertilizedSoilID, Material.grass).setCreativeTab(GrowableResources.tabGrowableResources).setHardness(0.7F).setUnlocalizedName("blockFertilizedSoilID");
        MinecraftForge.setBlockHarvestLevel(blockFertilizedSoil, "shovel", 1);
        GameRegistry.registerBlock(blockFertilizedSoil, "blockFertilizedSoil");
        LanguageRegistry.addName(blockFertilizedSoil, "Fertilized Soil");

        blockShredder = new BlockShredder(GRConfig.blockShredderID, Material.iron).setCreativeTab(GrowableResources.tabGrowableResources).setHardness(3F).setResistance(2F).setUnlocalizedName("blockShredder");
        MinecraftForge.setBlockHarvestLevel(blockShredder, "pickaxe", 2);
        GameRegistry.registerBlock(blockShredder, "blockShredder");
        LanguageRegistry.addName(blockShredder, "Reed Shredder");

        blockInfuser = new BlockInfuser(GRConfig.blockInfuserID).setCreativeTab(GrowableResources.tabGrowableResources).setHardness(3F).setResistance(2F).setUnlocalizedName("blockInfuser");
        MinecraftForge.setBlockHarvestLevel(blockInfuser, "pickaxe", 2);
        GameRegistry.registerBlock(blockInfuser, "blockInfuser");
        LanguageRegistry.addName(blockInfuser, "Reed Infuser");
        
        blockMiniBlock = new BlockMiniatureBlock(GRConfig.blockMiniBlockID).setCreativeTab(GrowableResources.tabGrowableResources).setHardness(2F).setResistance(1.5F).setUnlocalizedName("blockMiniBlock");
        GameRegistry.registerBlock(blockMiniBlock, "blockMiniBlock");
        LanguageRegistry.addName(blockMiniBlock, "Miniature Block");
    }

    public static void registerTileEntities() {
        TileEntity.addMapping(TileEntityResourceReeds.class, "gr.resourceReeds");
        TileEntity.addMapping(TileEntityShredder.class, "gr.shredder");
        TileEntity.addMapping(TileEntityInfuser.class, "gr.infuser");
        TileEntity.addMapping(TileEntityMiniatureBlock.class, "gr.miniBlock");
    }
}
