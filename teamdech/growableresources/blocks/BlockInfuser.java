package teamdech.growableresources.blocks;

import cpw.mods.fml.common.network.FMLNetworkHandler;
import net.minecraft.block.BlockContainer;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Icon;
import net.minecraft.world.World;
import teamdech.growableresources.GrowableResources;
import teamdech.growableresources.util.GRUtil;

public class BlockInfuser extends BlockContainer {
    public Icon[] textures = new Icon[3];

    public BlockInfuser(int blockID) {
        super(blockID, Material.iron);
    }

    @Override
    public TileEntity createNewTileEntity(World world) {
        return new TileEntityInfuser();
    }

    @Override
    public Icon getIcon(int side, int meta) {
        return side == 0 ? textures[0] : (side == 1 ? textures[1] : textures[2]);
    }

    @Override
    public void registerIcons(IconRegister iconRegister) {
        textures[0] = iconRegister.registerIcon("growableresources:infuser_bottom");
        textures[1] = iconRegister.registerIcon("growableresources:infuser_top");
        textures[2] = iconRegister.registerIcon("growableresources:infuser_sides");
    }

    @Override
    public boolean onBlockActivated(World world, int x, int y, int z, EntityPlayer player, int par6, float par7, float par8, float par9) {
        if (!world.isRemote) {
            FMLNetworkHandler.openGui(player, GrowableResources.INSTANCE, 1, world, x, y, z);
        }
        return true;
    }

    @Override
    public boolean removeBlockByPlayer(World world, EntityPlayer player, int x, int y, int z) {
        TileEntityInfuser tileInfuser = (TileEntityInfuser) world.getBlockTileEntity(x, y, z);
        if (tileInfuser == null) return true;
        if (!world.isRemote && (player == null || !player.capabilities.isCreativeMode))
            for (int i = 0; i < tileInfuser.getSizeInventory(); i++)
                if (tileInfuser.getStackInSlot(i) != null)
                    GRUtil.spawnItemInWorld(world, tileInfuser.getStackInSlot(i), x, y, z, 0, 0);
        super.removeBlockByPlayer(world, player, x, y, z);
        return true;
    }
}
