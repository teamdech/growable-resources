package teamdech.growableresources.gui;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.world.World;

/**
 * User: joel / Date: 20.12.13 / Time: 17:30
 */
public class ContainerInfoTool extends Container {

    public int x, y, z;
    public World world;

    public ContainerInfoTool(int x, int y, int z, World world) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.world = world;
    }

    @Override
    public boolean canInteractWith(EntityPlayer entityplayer) {
        return true;
    }
}
