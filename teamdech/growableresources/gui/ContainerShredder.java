package teamdech.growableresources.gui;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.ICrafting;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import teamdech.growableresources.blocks.TileEntityShredder;

public class ContainerShredder extends Container {
    public TileEntityShredder shredder;
    public int lastUpdatedFuelLevel = 0;
    public int lastUpdatedProgress = 0;
    public int lastUpdatedMaxFuelLevel = 0;

    public ContainerShredder(InventoryPlayer inventoryPlayer, TileEntity tileEntity) {
        shredder = (TileEntityShredder) tileEntity;

        for (int x = 0; x < 9; x++) {
            addSlotToContainer(new Slot(inventoryPlayer, x, 8 + 18 * x, 142));
        }
        for (int y = 0; y < 3; y++) {
            for (int x = 0; x < 9; x++) {
                addSlotToContainer(new Slot(inventoryPlayer, x + y * 9 + 9, 8 + 18 * x, 84 + y * 18));
            }
        }

        addSlotToContainer(new SlotShredder(shredder, 0, 56, 21, "INPUT"));
        addSlotToContainer(new SlotShredder(shredder, 1, 56, 57, "FUEL"));
        addSlotToContainer(new SlotShredder(shredder, 2, 116, 39, "OUTPUT"));
    }

    @Override
    public boolean canInteractWith(EntityPlayer entityplayer) {
        return shredder.isUseableByPlayer(entityplayer);
    }

    @Override
    public void addCraftingToCrafters(ICrafting player) {
        super.addCraftingToCrafters(player);

        player.sendProgressBarUpdate(this, 0, shredder.currentProgress);
        player.sendProgressBarUpdate(this, 1, shredder.currentFuelLevel);
        player.sendProgressBarUpdate(this, 2, shredder.maxFuelLevel);
    }

    @Override
    public void detectAndSendChanges() {
        super.detectAndSendChanges();

        if (shredder.currentProgress % 10 == 0 && shredder.currentProgress / 10 != lastUpdatedProgress) {
            for (Object player : crafters)
                ((ICrafting) player).sendProgressBarUpdate(this, 0, shredder.currentProgress);
            lastUpdatedProgress = shredder.currentProgress;
        }
        if (shredder.currentFuelLevel % 10 == 0 && shredder.currentFuelLevel / 10 != lastUpdatedFuelLevel) {
            for (Object player : crafters)
                ((ICrafting) player).sendProgressBarUpdate(this, 1, shredder.currentFuelLevel);
            lastUpdatedFuelLevel = shredder.currentFuelLevel;
        }
        if (shredder.maxFuelLevel != lastUpdatedMaxFuelLevel) {
            for (Object player : crafters) ((ICrafting) player).sendProgressBarUpdate(this, 2, shredder.maxFuelLevel);
            lastUpdatedMaxFuelLevel = shredder.maxFuelLevel;
        }
    }

    @Override
    public void updateProgressBar(int channel, int data) {
        if (channel == 0) shredder.currentProgress = data;
        if (channel == 1) shredder.currentFuelLevel = data;
        if (channel == 2) shredder.maxFuelLevel = data;
    }

    @Override
    public ItemStack transferStackInSlot(EntityPlayer player, int slotID) {
        return null;
    }
}
