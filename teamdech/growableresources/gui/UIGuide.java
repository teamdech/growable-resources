package teamdech.growableresources.gui;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.inventory.Container;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

/**
 * User: joel / Date: 16.12.13 / Time: 22:34
 */
public class UIGuide extends GuiContainer {

    public int page;

    public UIGuide(Container container) {
        super(container);

        xSize = 202;
        ySize = 256;

        page = 0;
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float f, int i, int j) {
        GL11.glColor3f(1, 1, 1);

        Minecraft.getMinecraft().getTextureManager().bindTexture(new ResourceLocation("growableresources", "textures/gui/guiGuidePage" + page + ".png"));
        drawTexturedModalRect(guiLeft, guiTop, 0, 0, xSize, ySize);
    }

    @Override
    protected void mouseClicked(int x, int y, int button) {
        x -= guiLeft;
        y -= guiTop;

        if (y > 235 && y < 250) {
            if (x > 8 && x < 66 && page > 0) page--;
            if (x > 150 && x < 195 && page < 6) page++;
        }
    }
}
