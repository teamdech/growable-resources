package teamdech.growableresources.gui;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import teamdech.growableresources.blocks.TileEntityInfuser;
import teamdech.growableresources.items.Items;

public class SlotInfuser extends Slot {
    public String type;
    public TileEntityInfuser infuser;

    public SlotInfuser(TileEntityInfuser infuser, int slotID, int x, int y, String slotType) {
        super(infuser, slotID, x, y);

        this.infuser = infuser;
        type = slotType;
    }

    @Override
    public boolean isItemValid(ItemStack item) {
        return !infuser.locked && (type == "MATERIAL" || (type == "INPUT" && item.itemID == Items.itemReedRoots.itemID) || (type == "FUEL" && item.itemID == Items.itemInfusionFuel.itemID));
    }

    @Override
    public boolean canTakeStack(EntityPlayer par1EntityPlayer) {
        return !infuser.locked;
    }

    @Override
    public int getSlotStackLimit() {
        return 1;
    }
}
