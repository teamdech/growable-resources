package teamdech.growableresources.gui;

import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntityFurnace;
import teamdech.growableresources.items.Items;

public class SlotShredder extends Slot {
    public String slotType;

    public SlotShredder(IInventory tile, int x, int y, int slotID, String type) {
        super(tile, x, y, slotID);

        slotType = type;
    }

    @Override
    public boolean isItemValid(ItemStack item) {
        return slotType == "OUTPUT" ? false : (slotType == "INPUT" ? item.itemID == Items.itemReeds.itemID : TileEntityFurnace.isItemFuel(item));
    }
}
