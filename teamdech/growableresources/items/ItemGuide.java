package teamdech.growableresources.items;

import cpw.mods.fml.common.network.FMLNetworkHandler;
import net.minecraft.client.renderer.texture.IconRegister;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import teamdech.growableresources.GrowableResources;

/**
 * User: joel / Date: 16.12.13 / Time: 22:17
 */
// TODO max stack size to 1
public class ItemGuide extends Item {

    public ItemGuide(int ID) {
        super(ID);
        setUnlocalizedName("itemGuide");
    }

    @Override
    public void registerIcons(IconRegister iconRegister) {
        itemIcon = iconRegister.registerIcon("growableresources:guide");
    }

    @Override
    public ItemStack onItemRightClick(ItemStack itemStack, World world, EntityPlayer entityPlayer) {
        FMLNetworkHandler.openGui(entityPlayer, GrowableResources.INSTANCE, 2, world, 0, 0, 0);
        return super.onItemRightClick(itemStack, world, entityPlayer);
    }
}
