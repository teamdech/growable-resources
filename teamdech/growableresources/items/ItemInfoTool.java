package teamdech.growableresources.items;

import cpw.mods.fml.common.network.FMLNetworkHandler;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import teamdech.growableresources.GrowableResources;
import teamdech.growableresources.blocks.Blocks;
import teamdech.growableresources.blocks.TileEntityResourceReeds;

/**
 * User: joel / Date: 20.12.13 / Time: 17:39
 */
public class ItemInfoTool extends Item {

    public ItemInfoTool(int id) {
        super(id);
    }

    @Override
    public boolean onItemUseFirst(ItemStack stack, EntityPlayer player, World world, int x, int y, int z, int side, float hitX, float hitY, float hitZ) {
        if (world.getBlockId(x, y, z) == Blocks.blockReeds.blockID) {
            if (world.getBlockMetadata(x, y, z) == 15) y--;
            if (world.getBlockId(x, y, z) == Blocks.blockReeds.blockID && world.getBlockMetadata(x, y, z) == 15) y--;
            if (world.getBlockTileEntity(x, y, z) instanceof TileEntityResourceReeds)
                FMLNetworkHandler.openGui(player, GrowableResources.INSTANCE, 3, world, x, y, z);
        }

	/*
     * if (world.getBlockTileEntity(x, y, z) instanceof
	 * TileEntityResourceReeds) FMLNetworkHandler.openGui(player,
	 * GrowableResources.INSTANCE, 3, world, x, y, z);
	 */
        return true;
    }
}